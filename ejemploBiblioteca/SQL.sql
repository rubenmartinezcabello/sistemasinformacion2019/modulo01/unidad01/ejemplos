﻿ /* Crear bases de datos para los siguientes casos de la relación
  *
  * [ejemplares] -- <presta> -- [socios]
  *
  * socios:
  *   71421914 Rubén
  *   43785421 Rosa
  *
  * ejemplares:
  *   666 Biblia
  *   193 Quijote
  *   104 Celestina
  */

-- [ejemplares](N) -- <presta> -- (N)[socios]
DROP DATABASE IF EXISTS `ejemploBiblioteca_opcion1`;
CREATE DATABASE `ejemploBiblioteca_opcion1`; -- N x N  (3 tablas)
USE `ejemploBiblioteca_opcion1`;


CREATE TABLE socios (
  codSocio int (8),
  PRIMARY KEY (codSocio)
  );

CREATE TABLE ejemplares (
  codEjemplar int (9),
  PRIMARY KEY (codEjemplar)
  );

CREATE TABLE presta (
  refSocio int(8),
  refEjemplar int(9),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (refSocio,refEjemplar),
  FOREIGN KEY (refSocio) REFERENCES socios (codSocio) ,
  FOREIGN KEY (refEjemplar) REFERENCES ejemplares (codEjemplar)
  );

-- Valores de prueba

INSERT INTO socios (codSocio) VALUES ('71421914'), ('43785421'); -- Rubén, Rosa
INSERT INTO ejemplares (codEjemplar) VALUES (666),(193),(104); -- Biblia, Quijote, Celestina
INSERT INTO presta(refSocio, refEjemplar) VALUE ('71421914',193); -- Ruben, Quijote
INSERT INTO presta(refSocio, refEjemplar) VALUE ('43785421',193); -- Rosa, Quijote
INSERT INTO presta(refSocio, refEjemplar) VALUE ('71421914',104); -- Ruben, Celestina
-- INSERT INTO presta(refSocio, refEjemplar) VALUE ('71421914',193); -- Ruben, Quijote

-- *********************************************************************************************

-- [ejemplares](1) -- <presta> -- (N)[socios]
DROP DATABASE IF EXISTS `ejemploBiblioteca_opcion2`;
CREATE DATABASE `ejemploBiblioteca_opcion2`; -- 1 x N  (3 tablas)
USE `ejemploBiblioteca_opcion2`;


CREATE TABLE socios (
  codSocio int (8),
  PRIMARY KEY (codSocio)
  );

CREATE TABLE ejemplares (
  codEjemplar int (9),
  PRIMARY KEY (codEjemplar)
  );

CREATE TABLE presta (
  refSocio int(8),
  refEjemplar int(9),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (refSocio,refEjemplar),
  UNIQUE KEY (refSocio),
  FOREIGN KEY (refSocio) REFERENCES socios (codSocio),
  FOREIGN KEY (refEjemplar) REFERENCES ejemplares (codEjemplar)
  );


INSERT INTO socios (codSocio) VALUES ('71421914'), ('43785421'); -- Rubén, Rosa
INSERT INTO ejemplares (codEjemplar) VALUES (666),(193),(104); -- Biblia, Quijote, Celestina
INSERT INTO presta(refSocio, refEjemplar) VALUE ('71421914',193); -- Ruben, Quijote
INSERT INTO presta(refSocio, refEjemplar) VALUE ('43785421',193); -- Rosa, Quijote
-- INSERT INTO presta(refSocio, refEjemplar) VALUE ('71421914',104); -- Ruben, Celestina
-- INSERT INTO presta(refSocio, refEjemplar) VALUE ('71421914',193); -- Ruben, Quijote


-- *********************************************************************************************

-- [ejemplares](N) -- <presta> -- (1)[socios]
DROP DATABASE IF EXISTS `ejemploBiblioteca_opcion3`;
CREATE DATABASE `ejemploBiblioteca_opcion3`; -- N x 1  (3 tablas)
USE `ejemploBiblioteca_opcion3`;


CREATE TABLE socios (
  codSocio int (8),
  PRIMARY KEY (codSocio)
  );

CREATE TABLE ejemplares (
  codEjemplar int (9),
  PRIMARY KEY (codEjemplar)
  );

CREATE TABLE presta (
  refSocio int(8),
  refEjemplar int(9),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (refSocio,refEjemplar),
  UNIQUE KEY (refEjemplar),
  FOREIGN KEY (refSocio) REFERENCES socios (codSocio),
  FOREIGN KEY (refEjemplar) REFERENCES ejemplares (codEjemplar)
  );


INSERT INTO socios (codSocio) VALUES ('71421914'), ('43785421'); -- Rubén, Rosa
INSERT INTO ejemplares (codEjemplar) VALUES (666),(193),(104); -- Biblia, Quijote, Celestina
INSERT INTO presta(refSocio, refEjemplar) VALUE ('71421914',193); -- Ruben, Quijote
-- INSERT INTO presta(refSocio, refEjemplar) VALUE ('43785421',193); -- Rosa, Quijote
INSERT INTO presta(refSocio, refEjemplar) VALUE ('71421914',104); -- Ruben, Celestina
-- INSERT INTO presta(refSocio, refEjemplar) VALUE ('71421914',193); -- Ruben, Quijote


-- *********************************************************************************************

-- [ejemplares](1) -- <presta> -- (1)[socios]
DROP DATABASE IF EXISTS `ejemploBiblioteca_opcion4`;
CREATE DATABASE `ejemploBiblioteca_opcion4`; -- 1 x 1  (3 tablas)
USE `ejemploBiblioteca_opcion4`;


CREATE TABLE socios (
  codSocio int (8),
  PRIMARY KEY (codSocio)
  );

CREATE TABLE ejemplares (
  codEjemplar int (9),
  PRIMARY KEY (codEjemplar)
  );

CREATE TABLE presta (
  refSocio int(8),
  refEjemplar int(9),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (refSocio,refEjemplar),
  UNIQUE KEY (refSocio),
  UNIQUE KEY (refEjemplar),
  FOREIGN KEY (refSocio) REFERENCES socios (codSocio),
  FOREIGN KEY (refEjemplar) REFERENCES ejemplares (codEjemplar)
  );


INSERT INTO socios (codSocio) VALUES ('71421914'), ('43785421'); -- Rubén, Rosa
INSERT INTO ejemplares (codEjemplar) VALUES (666),(193),(104); -- Biblia, Quijote, Celestina
INSERT INTO presta(refSocio, refEjemplar) VALUE ('71421914',193); -- Ruben, Quijote
-- INSERT INTO presta(refSocio, refEjemplar) VALUE ('43785421',193); -- Rosa, Quijote
-- INSERT INTO presta(refSocio, refEjemplar) VALUE ('71421914',104); -- Ruben, Celestina
-- INSERT INTO presta(refSocio, refEjemplar) VALUE ('71421914',193); -- Ruben, Quijote



-- *********************************************************************************************

-- [ejemplares](1) -- <presta> -- (N)[socios]
DROP DATABASE IF EXISTS `ejemploBiblioteca_opcion5`;
CREATE DATABASE `ejemploBiblioteca_opcion5`; -- 1 x N  (Simplificacion a 2 tablas por propagación a N)
USE `ejemploBiblioteca_opcion5`;


CREATE TABLE ejemplares (
  codEjemplar int (9),
  PRIMARY KEY (codEjemplar)
  );

CREATE TABLE socios (
  codSocio int (8),
  refEjemplar int(9),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (codSocio),
  FOREIGN KEY (refEjemplar) REFERENCES ejemplares (codEjemplar)
  );

-- TABLE presta propagada a socios(N)

INSERT INTO ejemplares (codEjemplar) VALUES (666),(193),(104); -- Biblia, Quijote, Celestina
INSERT INTO  socios (codSocio, refEjemplar) VALUE ('71421914', 193); -- Ruben, Quijote
INSERT INTO  socios (codSocio, refEjemplar) VALUE ('43785421', 193); -- Rosa, Celestina



-- *********************************************************************************************

-- [ejemplares](N) -- <presta> -- (1)[socios]
DROP DATABASE IF EXISTS `ejemploBiblioteca_opcion6`;
CREATE DATABASE `ejemploBiblioteca_opcion6`; -- N x 1  (Simplificacion a 2 tablas por propagación a N)
USE `ejemploBiblioteca_opcion6`;

CREATE TABLE socios (
  codSocio int (8),
  PRIMARY KEY (codSocio)
  );

CREATE TABLE ejemplares (
  codEjemplar int (9),
  refSocio int(8),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (codEjemplar),
  FOREIGN KEY (refSocio) REFERENCES socios (codSocio)
  );

-- TABLE presta propagada a ejemplares(N)
INSERT INTO socios (codSocio) VALUES ('71421914'), ('43785421'); -- Rubén, Rosa
INSERT INTO ejemplares (codEjemplar, refSocio) VALUE (193, '71421914'); -- Ruben, Quijote
INSERT INTO ejemplares (codEjemplar, refSocio) VALUE (104, '71421914'); -- Ruben, Celestina
