﻿ /* Crear bases de datos para los siguientes casos de la relación
  *
  * [ejemplares] -- <presta> -- [socios]
  *
  * socios:
  *   71421914 Rubén
  *   43785421 Rosa
  *
  * ejemplares:
  *   666 Biblia
  *   193 Quijote
  *   104 Celestina
  */

-- [ejemplares](N) -- <presta> -- (1)[socios]
DROP DATABASE IF EXISTS `ejemploBiblioteca_opcion6`;
CREATE DATABASE `ejemploBiblioteca_opcion6`; -- N x 1  (Simplificacion a 2 tablas por propagación a N)
USE `ejemploBiblioteca_opcion6`;


CREATE TABLE socios (
  codSocio int (8),
  PRIMARY KEY (codSocio)
  );

CREATE TABLE ejemplares (
  codEjemplar int (9),
  refSocio int(8),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (codEjemplar),
  FOREIGN KEY (refSocio) REFERENCES socios (codSocio)
  );

-- TABLE presta propagada a ejemplares(N)

-- Valores de prueba

INSERT INTO socios (codSocio) VALUES ('71421914'), ('43785421'); -- Rubén, Rosa
INSERT INTO ejemplares (codEjemplar, refSocio) VALUE (193, '71421914'); -- Ruben, Quijote
-- INSERT INTO ejemplares (codEjemplar, refSocio) VALUE (193, '43785421'); -- Rosa, Quijote
INSERT INTO ejemplares (codEjemplar, refSocio) VALUE (104, '71421914'); -- Ruben, Celestina
-- INSERT INTO ejemplares (codEjemplar, refSocio) VALUE (193, '71421914'); -- Ruben, Quijote

