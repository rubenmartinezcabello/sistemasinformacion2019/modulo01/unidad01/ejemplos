﻿ /* Crear bases de datos para los siguientes casos de la relación
  *
  * [ejemplares] -- <presta> -- [socios]
  *
  * socios:
  *   71421914 Rubén
  *   43785421 Rosa
  *
  * ejemplares:
  *   666 Biblia
  *   193 Quijote
  *   104 Celestina
  */

-- [ejemplares](1) -- <presta> -- (N)[socios]
DROP DATABASE IF EXISTS `ejemploBiblioteca_opcion5`;
CREATE DATABASE `ejemploBiblioteca_opcion5`; -- 1 x N  (Simplificacion a 2 tablas por propagación a N)
USE `ejemploBiblioteca_opcion5`;


CREATE TABLE ejemplares (
  codEjemplar int (9),
  PRIMARY KEY (codEjemplar)
  );

CREATE TABLE socios (
  codSocio int (8),
  refEjemplar int(9),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (codSocio),
  FOREIGN KEY (refEjemplar) REFERENCES ejemplares (codEjemplar)
  );

-- TABLE presta propagada a socios(N)

-- Valores de prueba

INSERT INTO ejemplares (codEjemplar) VALUES (666),(193),(104); -- Biblia, Quijote, Celestina
INSERT INTO  socios (codSocio, refEjemplar) VALUE ('71421914', 193); -- Ruben, Quijote
INSERT INTO  socios (codSocio, refEjemplar) VALUE ('43785421', 193); -- Rosa, Quijote
-- INSERT INTO  socios (codSocio, refEjemplar) VALUE ('71421914', 104); -- Ruben, Celestina
-- INSERT INTO  socios (codSocio, refEjemplar) VALUE ('71421914', 193); -- Ruben, Quijote

