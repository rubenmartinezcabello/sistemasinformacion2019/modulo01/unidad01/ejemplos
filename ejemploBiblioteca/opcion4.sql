﻿ /* Crear bases de datos para los siguientes casos de la relación
  *
  * [ejemplares] -- <presta> -- [socios]
  *
  * socios:
  *   71421914 Rubén
  *   43785421 Rosa
  *
  * ejemplares:
  *   666 Biblia
  *   193 Quijote
  *   104 Celestina
  */

-- [ejemplares](1) -- <presta> -- (1)[socios]
DROP DATABASE IF EXISTS `ejemploBiblioteca_opcion4`;
CREATE DATABASE `ejemploBiblioteca_opcion4`; -- 1 x 1  (3 tablas)
USE `ejemploBiblioteca_opcion4`;


CREATE TABLE socios (
  codSocio int (8),
  PRIMARY KEY (codSocio)
  );

CREATE TABLE ejemplares (
  codEjemplar int (9),
  PRIMARY KEY (codEjemplar)
  );

CREATE TABLE presta (
  refSocio int(8),
  refEjemplar int(9),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (refSocio,refEjemplar),
  UNIQUE KEY (refSocio),
  UNIQUE KEY (refEjemplar),
  FOREIGN KEY (refSocio) REFERENCES socios (codSocio),
  FOREIGN KEY (refEjemplar) REFERENCES ejemplares (codEjemplar)
  );

-- Valores de prueba

INSERT INTO socios (codSocio) VALUES ('71421914'), ('43785421'); -- Rubén, Rosa
INSERT INTO ejemplares (codEjemplar) VALUES (666),(193),(104); -- Biblia, Quijote, Celestina
INSERT INTO presta(refSocio, refEjemplar) VALUE ('71421914',193); -- Ruben, Quijote
-- INSERT INTO presta(refSocio, refEjemplar) VALUE ('43785421',193); -- Rosa, Quijote
-- INSERT INTO presta(refSocio, refEjemplar) VALUE ('71421914',104); -- Ruben, Celestina
-- INSERT INTO presta(refSocio, refEjemplar) VALUE ('71421914',193); -- Ruben, Quijote

