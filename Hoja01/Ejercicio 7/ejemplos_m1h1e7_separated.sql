-- Ejercicio 7

-- DATABASE -- BEGIN --
DROP DATABASE IF EXISTS ejemplos_m1h1e7;
CREATE DATABASE ejemplos_m1h1e7;
-- DATABASE -- END --

USE ejemplos_m1h1e7;

-- TABLES -- BEGIN --
CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE TelefonosClientes (
  idCliente int,
  telefono int (9),
  PRIMARY KEY (idCliente, telefono)
  );

CREATE TABLE Tiendas (
  codigo int,
  direccion varchar (128),
  PRIMARY KEY (codigo)
  );

CREATE TABLE compran (
  idProducto int, 
  codigoTienda int,
  idCliente int, 
  PRIMARY KEY (idProducto, codigoTienda, idCliente),
  UNIQUE KEY (idProducto, codigoTienda), -- Los Ns
  FOREIGN KEY (idProducto) REFERENCES Productos(id),
  FOREIGN KEY (codigoTienda) REFERENCES Tiendas(codigo),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );

CREATE TABLE compranFechas (
  idProducto int, 
  codigoTienda int,
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, codigoTienda, idCliente, fecha)
  );
-- TABLES --  END --

-- CONSTRAINTS -- BEGIN --
ALTER TABLE TelefonosClientes
  ADD CONSTRAINT fk_telefonosClientes_id
    FOREIGN KEY (idCliente) REFERENCES Clientes(id);

ALTER TABLE compran
  ADD CONSTRAINT uk_compran_ProductoTienda
    UNIQUE KEY (idProducto, codigoTienda), -- Los Ns
  ADD CONSTRAINT fk_compran_producto_id
    FOREIGN KEY (idProducto) REFERENCES Productos(id),
  ADD CONSTRAINT fk_compran_tienda_codigo
    FOREIGN KEY (codigoTienda) REFERENCES Tiendas(codigo),
  ADD CONSTRAINT fk_compran_cliente_id
    FOREIGN KEY (idCliente) REFERENCES Clientes(id);
	
ALTER TABLE compranFechas
  ADD CONSTRAINT fk_compranFechas_compran_pk
    FOREIGN KEY (idProducto, codigoTienda, idCliente) REFERENCES compran(idProducto, codigoTienda, idCliente);
-- CONSTRAINTS -- END --	