-- Ejercicio 7

-- BEGIN --
DROP DATABASE IF EXISTS ejemplos_m1h1e7;
CREATE DATABASE ejemplos_m1h1e7;
USE ejemplos_m1h1e7;

CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE TelefonosClientes (
  idCliente int,
  telefono int (9),
  PRIMARY KEY (idCliente, telefono),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );

CREATE TABLE Tiendas (
  codigo int,
  direccion varchar (128),
  PRIMARY KEY (codigo)
  );

CREATE TABLE compran (
  idProducto int, 
  codigoTienda int,
  idCliente int, 
  PRIMARY KEY (idProducto, codigoTienda, idCliente),
  UNIQUE KEY (idProducto, codigoTienda), -- Los Ns
  FOREIGN KEY (idProducto) REFERENCES Productos(id),
  FOREIGN KEY (codigoTienda) REFERENCES Tiendas(codigo),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );

CREATE TABLE compranFechas (
  idProducto int, 
  codigoTienda int,
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, codigoTienda, idCliente, fecha),
  FOREIGN KEY (idProducto, codigoTienda, idCliente) REFERENCES compran(idProducto, codigoTienda, idCliente)
  );
-- END --