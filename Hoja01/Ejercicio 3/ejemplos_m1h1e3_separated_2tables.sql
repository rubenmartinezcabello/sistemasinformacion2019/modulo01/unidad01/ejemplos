-- Ejercicio 3 (2 tablas)

-- DATABASE -- BEGIN --
DROP DATABASE IF EXISTS ejemplos_m1h1e3_2tablas;
CREATE DATABASE ejemplos_m1h1e3_2tablas;
-- DATABASE -- END --

USE ejemplos_m1h1e3_2tablas;

-- TABLES -- BEGIN --
CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  idCliente int,
  fecha date, 
  cantidad int,
  PRIMARY KEY (id)
  );
-- TABLES --  END --

-- CONSTRAINTS -- BEGIN --
ALTER TABLE Productos
  ADD CONSTRAINT uk_idCliente
    UNIQUE KEY (idCliente),
  ADD CONSTRAINT fk_clientes_id 
    FOREIGN KEY (idCliente) REFERENCES Clientes(id);
-- CONSTRAINTS -- END --