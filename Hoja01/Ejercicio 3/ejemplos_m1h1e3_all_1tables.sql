-- Ejercicio 3

-- BEGIN -- Ejercicio 3 (1 tablas)
DROP DATABASE IF EXISTS ejemplos_m1h1e3_1tabla;
CREATE DATABASE ejemplos_m1h1e3_1tabla;
USE ejemplos_m1h1e3_1tabla;

CREATE TABLE ClientesCompranProductos (
  idProducto int,
  nombreProducto varchar(64),
  pesoProducto float(5,2),
  idCliente int,
  nombreCliente varchar(64),
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, idCliente),
  UNIQUE KEY (idProducto),
  UNIQUE KEY (idCliente)
  );
-- END -- Ejercicio 3 (1 tablas)