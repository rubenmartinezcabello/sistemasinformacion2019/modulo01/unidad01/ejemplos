-- Ejercicio 3 (1 tablas)

-- DATABASE -- BEGIN --
DROP DATABASE IF EXISTS ejemplos_m1h1e3_1tabla;
CREATE DATABASE ejemplos_m1h1e3_1tabla;
-- DATABASE -- END --

USE ejemplos_m1h1e3_1tabla;

-- TABLES -- BEGIN --
CREATE TABLE ClientesCompranProductos (
  idProducto int,
  nombreProducto varchar(64),
  pesoProducto float(5,2),
  idCliente int,
  nombreCliente varchar(64),
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, idCliente)
  );
-- TABLES --  END --

-- CONSTRAINTS -- BEGIN --
ALTER TABLE ClientesCompranProductos
  ADD CONSTRAINT uk_idCliente
    UNIQUE KEY (idCliente),
  ADD CONSTRAINT uk_idProducto
    UNIQUE KEY (idProducto);
-- CONSTRAINTS -- END --