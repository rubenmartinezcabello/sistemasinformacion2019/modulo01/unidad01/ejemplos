-- Ejercicio 3 (3 tablas)

-- DATABASE -- BEGIN --
DROP DATABASE IF EXISTS ejemplos_m1h1e3_3tablas;
CREATE DATABASE ejemplos_m1h1e3_3tablas;
-- DATABASE -- END --

USE ejemplos_m1h1e3_3tablas;

-- TABLES -- BEGIN --
CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );
  
CREATE TABLE compran (
  idProducto int, 
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, idCliente)
  );
-- TABLES --  END --

-- CONSTRAINTS -- BEGIN --
ALTER TABLE compran
  ADD CONSTRAINT uk_idProducto
    UNIQUE KEY (idProducto),
  ADD CONSTRAINT uk_idCliente
    UNIQUE KEY (idCliente),
  ADD CONSTRAINT fk_productos_id 
    FOREIGN KEY (idProducto) REFERENCES Productos(id),
  ADD CONSTRAINT fk_clientes_id 
    FOREIGN KEY (idCliente) REFERENCES Clientes(id);
-- CONSTRAINTS -- END --