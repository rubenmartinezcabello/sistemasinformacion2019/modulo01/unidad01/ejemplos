-- Ejercicio 3

-- BEGIN -- Ejercicio 3 (3 tablas)
DROP DATABASE IF EXISTS ejemplos_m1h1e3_3tablas;
CREATE DATABASE ejemplos_m1h1e3_3tablas;
USE ejemplos_m1h1e3_3tablas;

CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );
CREATE TABLE compran (
  idProducto int, 
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, idCliente),
  UNIQUE KEY (idProducto),
  UNIQUE KEY (idCliente),
  FOREIGN KEY (idProducto) REFERENCES Productos(id),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );
-- END -- Ejercicio 3 (3 tablas)
