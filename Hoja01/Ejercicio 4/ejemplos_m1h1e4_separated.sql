-- Ejercicio 4

-- DATABASE -- BEGIN --
DROP DATABASE IF EXISTS ejemplos_m1h1e4;
CREATE DATABASE ejemplos_m1h1e4;
-- DATABASE -- END --

USE ejemplos_m1h1e4;

-- TABLES -- BEGIN --
CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE TelefonosClientes (
  idCliente int,
  telefono int (9),
  PRIMARY KEY (idCliente, telefono)
  );

CREATE TABLE compran (
  idProducto int, 
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, idCliente)
  );
-- TABLES --  END --

-- CONSTRAINTS -- BEGIN --
ALTER TABLE TelefonosClientes
  ADD CONSTRAINT fk_telefonosClientes_id
    FOREIGN KEY (idCliente) REFERENCES Clientes(id);
	
ALTER TABLE compran
  ADD CONSTRAINT fk_productos_id 
    FOREIGN KEY (idProducto) REFERENCES Productos(id),
  ADD CONSTRAINT fk_clientes_id 
    FOREIGN KEY (idCliente) REFERENCES Clientes(id);
-- CONSTRAINTS -- END --