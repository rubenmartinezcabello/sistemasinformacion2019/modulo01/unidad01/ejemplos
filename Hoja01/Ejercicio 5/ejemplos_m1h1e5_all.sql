-- Ejercicio 5

-- BEGIN --
DROP DATABASE IF EXISTS ejemplos_m1h1e5;
CREATE DATABASE ejemplos_m1h1e5;
USE ejemplos_m1h1e5;

CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE TelefonosClientes (
  idCliente int,
  telefono int (9),
  PRIMARY KEY (idCliente, telefono),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );

CREATE TABLE compran (
  idProducto int, 
  idCliente int, 
  PRIMARY KEY (idProducto, idCliente),
  FOREIGN KEY (idProducto) REFERENCES Productos(id),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );

CREATE TABLE compranFechas (
  idProducto int, 
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, idCliente, fecha),
  FOREIGN KEY (idProducto, idCliente) REFERENCES compran(idProducto, idCliente)
  );
-- END --