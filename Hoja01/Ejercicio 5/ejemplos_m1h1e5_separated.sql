-- Ejercicio 5

-- DATABASE -- BEGIN --
DROP DATABASE IF EXISTS ejemplos_m1h1e5;
CREATE DATABASE ejemplos_m1h1e5;
-- DATABASE -- END --

USE ejemplos_m1h1e5;

-- TABLES -- BEGIN --
CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE TelefonosClientes (
  idCliente int,
  telefono int (9),
  PRIMARY KEY (idCliente, telefono)
  );

CREATE TABLE compran (
  idProducto int, 
  idCliente int, 
  PRIMARY KEY (idProducto, idCliente)
  );

CREATE TABLE compranFechas (
  idProducto int, 
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, idCliente, fecha)
  );
-- TABLES --  END --

-- CONSTRAINTS -- BEGIN --
ALTER TABLE TelefonosClientes
  ADD CONSTRAINT fk_telefonosClientes_id
    FOREIGN KEY (idCliente) REFERENCES Clientes(id);
	
ALTER TABLE compran
  ADD CONSTRAINT fk_compranProductos_id
    FOREIGN KEY (idProducto) REFERENCES Productos(id),
  ADD CONSTRAINT fk_compranClientes_id
    FOREIGN KEY (idCliente) REFERENCES Clientes(id);

ALTER TABLE compranFechas	
  ADD CONSTRAINT fk_compranFechas_pk
    FOREIGN KEY (idProducto, idCliente) REFERENCES compran(idProducto, idCliente);
-- CONSTRAINTS -- END --