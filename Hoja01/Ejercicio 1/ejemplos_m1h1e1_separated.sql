-- Ejercicio 1

-- DATABASE -- BEGIN --
DROP DATABASE IF EXISTS ejemplos_m1h1e1;
CREATE DATABASE ejemplos_m1h1e1;
-- DATABASE -- END --

USE ejemplos_m1h1e1;

-- TABLES -- BEGIN --
CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE compran (
  idProducto int, 
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, idCliente)
  );
-- TABLES --  END --

-- CONSTRAINTS -- BEGIN --
ALTER TABLE compran
  ADD CONSTRAINT fk_productos_id 
	FOREIGN KEY (idProducto) REFERENCES Productos(id),
  ADD CONSTRAINT fk_clientes_id 
	FOREIGN KEY (idCliente) REFERENCES Clientes(id);
-- CONSTRAINTS -- END --