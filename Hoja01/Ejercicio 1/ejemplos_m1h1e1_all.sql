-- Ejercicio 1

-- BEGIN --
DROP DATABASE IF EXISTS ejemplos_m1h1e1;
CREATE DATABASE ejemplos_m1h1e1;
USE ejemplos_m1h1e1;

CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE compran (
  idProducto int, 
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, idCliente),
  FOREIGN KEY (idProducto) REFERENCES Productos(id),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );
-- END --