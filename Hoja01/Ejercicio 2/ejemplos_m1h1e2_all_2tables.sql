-- Ejercicio 2

-- BEGIN -- Ejercicio 2 (2 tablas)
DROP DATABASE IF EXISTS ejemplos_m1h1e2_2tablas;
CREATE DATABASE ejemplos_m1h1e2_2tablas;
USE ejemplos_m1h1e2_2tablas;

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  idCliente int,
  fecha date, 
  cantidad int,
  PRIMARY KEY (id),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );
-- END -- Ejercicio 2 (2 tablas)
