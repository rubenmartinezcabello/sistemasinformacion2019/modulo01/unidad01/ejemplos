﻿-- Ejercicio 1

-- BEGIN --
DROP DATABASE IF EXISTS ejemplos_m1h1e1;
CREATE DATABASE ejemplos_m1h1e1;
USE ejemplos_m1h1e1;

CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE compran (
  idProducto int, 
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, idCliente),
  FOREIGN KEY (idProducto) REFERENCES Productos(id),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );
-- END --



-- Ejercicio 2

-- BEGIN -- Ejercicio 2 (3 tablas)
DROP DATABASE IF EXISTS ejemplos_m1h1e2_3tablas;
CREATE DATABASE ejemplos_m1h1e2_3tablas;
USE ejemplos_m1h1e2_3tablas;

CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE compran (
  idProducto int, 
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, idCliente),
  UNIQUE KEY (idProducto),
  FOREIGN KEY (idProducto) REFERENCES Productos(id),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );
-- END -- Ejercicio 2 (3 tablas)

-- BEGIN -- Ejercicio 2 (2 tablas)
DROP DATABASE IF EXISTS ejemplos_m1h1e2_2tablas;
CREATE DATABASE ejemplos_m1h1e2_2tablas;
USE ejemplos_m1h1e2_2tablas;

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  idCliente int,
  fecha date, 
  cantidad int,
  PRIMARY KEY (id),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );
-- END -- Ejercicio 2 (2 tablas)



-- Ejercicio 3

-- BEGIN -- Ejercicio 3 (3 tablas)
DROP DATABASE IF EXISTS ejemplos_m1h1e3_3tablas;
CREATE DATABASE ejemplos_m1h1e3_3tablas;
USE ejemplos_m1h1e3_3tablas;

CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE compran (
  idProducto int, 
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, idCliente),
  UNIQUE KEY (idProducto),
  UNIQUE KEY (idCliente),
  FOREIGN KEY (idProducto) REFERENCES Productos(id),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );
-- END -- Ejercicio 3 (3 tablas)

-- BEGIN -- Ejercicio 3 (2 tablas)
DROP DATABASE IF EXISTS ejemplos_m1h1e3_2tablas;
CREATE DATABASE ejemplos_m1h1e3_2tablas;
USE ejemplos_m1h1e3_2tablas;

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  idCliente int,
  fecha date, 
  cantidad int,
  PRIMARY KEY (id),
  UNIQUE KEY (idCliente),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );
-- END -- Ejercicio 3 (3 tablas)

-- BEGIN -- Ejercicio 3 (1 tablas)
DROP DATABASE IF EXISTS ejemplos_m1h1e3_1tabla;
CREATE DATABASE ejemplos_m1h1e3_1tabla;
USE ejemplos_m1h1e3_1tabla;

CREATE TABLE ClientesCompranProductos (
  idProducto int,
  nombreProducto varchar(64),
  pesoProducto float(5,2),
  idCliente int,
  nombreCliente varchar(64),
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, idCliente),
  UNIQUE KEY (idProducto),
  UNIQUE KEY (idCliente)
  );
-- END -- Ejercicio 3 (1 tablas)



-- Ejercicio 4

-- BEGIN --
DROP DATABASE IF EXISTS ejemplos_m1h1e4;
CREATE DATABASE ejemplos_m1h1e4;
USE ejemplos_m1h1e4;

CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE TelefonosClientes (
  idCliente int,
  telefono int (9),
  PRIMARY KEY (idCliente, telefono),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );

CREATE TABLE compran (
  idProducto int, 
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, idCliente),
  FOREIGN KEY (idProducto) REFERENCES Productos(id),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );
-- END --



-- Ejercicio 5

-- BEGIN --
DROP DATABASE IF EXISTS ejemplos_m1h1e5;
CREATE DATABASE ejemplos_m1h1e5;
USE ejemplos_m1h1e5;

CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE TelefonosClientes (
  idCliente int,
  telefono int (9),
  PRIMARY KEY (idCliente, telefono),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );

CREATE TABLE compran (
  idProducto int, 
  idCliente int, 
  PRIMARY KEY (idProducto, idCliente),
  FOREIGN KEY (idProducto) REFERENCES Productos(id),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );

CREATE TABLE compranFechas (
  idProducto int, 
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, idCliente, fecha),
  FOREIGN KEY (idProducto, idCliente) REFERENCES compran(idProducto, idCliente)
  );
-- END --



-- Ejercicio 6

-- BEGIN --
DROP DATABASE IF EXISTS ejemplos_m1h1e6;
CREATE DATABASE ejemplos_m1h1e6;
USE ejemplos_m1h1e6;

CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE TelefonosClientes (
  idCliente int,
  telefono int (9),
  PRIMARY KEY (idCliente, telefono),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );

CREATE TABLE Tiendas (
  codigo int,
  direccion varchar (128),
  PRIMARY KEY (codigo)
  );

CREATE TABLE compran (
  idProducto int, 
  codigoTienda int,
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, codigoTienda, idCliente),
  UNIQUE KEY (idProducto, codigoTienda), -- Los Ns
  FOREIGN KEY (idProducto) REFERENCES Productos(id),
  FOREIGN KEY (codigoTienda) REFERENCES Tiendas(codigo),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );
-- END --



-- Ejercicio 7

-- BEGIN --
DROP DATABASE IF EXISTS ejemplos_m1h1e7;
CREATE DATABASE ejemplos_m1h1e7;
USE ejemplos_m1h1e7;

CREATE TABLE Productos (
  id int,
  nombre varchar(64),
  peso float(5,2),
  PRIMARY KEY (id)
  );

CREATE TABLE Clientes (
  id int,
  nombre varchar(64),
  PRIMARY KEY (id)
  );

CREATE TABLE TelefonosClientes (
  idCliente int,
  telefono int (9),
  PRIMARY KEY (idCliente, telefono),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );

CREATE TABLE Tiendas (
  codigo int,
  direccion varchar (128),
  PRIMARY KEY (codigo)
  );

CREATE TABLE compran (
  idProducto int, 
  codigoTienda int,
  idCliente int, 
  PRIMARY KEY (idProducto, codigoTienda, idCliente),
  UNIQUE KEY (idProducto, codigoTienda), -- Los Ns
  FOREIGN KEY (idProducto) REFERENCES Productos(id),
  FOREIGN KEY (codigoTienda) REFERENCES Tiendas(codigo),
  FOREIGN KEY (idCliente) REFERENCES Clientes(id)
  );

CREATE TABLE compranFechas (
  idProducto int, 
  codigoTienda int,
  idCliente int, 
  fecha date, 
  cantidad int,
  PRIMARY KEY (idProducto, codigoTienda, idCliente, fecha),
  FOREIGN KEY (idProducto, codigoTienda, idCliente) REFERENCES compran(idProducto, codigoTienda, idCliente)
  );
-- END --