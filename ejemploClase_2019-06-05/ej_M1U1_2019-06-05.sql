DROP DATABASE IF EXISTS `ej_M1U1_2019-06-05`;

CREATE DATABASE IF NOT EXISTS  `ej_M1U1_2019-06-05`;
USE  `ej_M1U1_2019-06-05`;



CREATE OR REPLACE TABLE Persona (
  dni varchar(9) NOT NULL,
  nombre varchar(64),
  PRIMARY KEY (dni)
  );

INSERT INTO Persona (dni, nombre) VALUES 
  ('71421914Z', 'Rub�n Mart�nez Cabello'),
  ('71421913J', '�lvaro Mart�nez Cabello'),
  ('ASDFasdf', 'Luis');



CREATE OR REPLACE TABLE Telefonos (
  dni varchar(9) NOT NULL,
  telefono integer(9),
  PRIMARY KEY (dni, telefono),
  FOREIGN KEY (dni) REFERENCES Persona(dni)
  );

INSERT INTO Telefonos (dni, telefono) VALUES 
  ('71421914Z', 639452663),
  ('71421913J', 666666666),
  ('71421913J', 987654321),
  ('71421914Z', 942942942);
  