DROP DATABASE IF EXISTS `ej_videoclub`;

CREATE DATABASE IF NOT EXISTS `ej_videoclub`;
USE  `ej_videoclub`;



CREATE OR REPLACE TABLE Socios (
  codigo int NOT NULL,
  nombre varchar(64),
  PRIMARY KEY (codigo)
  );

CREATE OR REPLACE TABLE Telefonos (
  codigoSocio int NOT NULL,
  telefono integer(9),
  PRIMARY KEY (codigoSocio, telefono),
  FOREIGN KEY (codigoSocio) REFERENCES Socios(codigo)
  );

CREATE OR REPLACE TABLE Emails (
  codigoSocio int NOT NULL,
  email varchar(64),
  PRIMARY KEY (codigoSocio, email),
  FOREIGN KEY (codigoSocio) REFERENCES Socios(codigo)
  );


INSERT INTO Socios (codigo, nombre) VALUES 
  (1, 'Rub�n Mart�nez Cabello'),
  (2, '�lvaro Mart�nez Cabello'),
  (3, 'Luis');

INSERT INTO Telefonos (codigoSocio, telefono) VALUES 
  (1, 639452663),
  (2, 666666666),
  (2, 987654321),
  (1, 942942942);

INSERT INTO Emails (codigoSocio, email) VALUES 
  (1, 'runar.orested@gmail.com'),
  (1, 'ad@martinezad.es'),
  (2, 'info@alvaronova.com'),
  (3, 'asdf@asdf.org');



CREATE OR REPLACE TABLE Peliculas (
  codigo varchar(13) NOT NULL,
  titulo varchar (64),
  PRIMARY KEY (codigo)
  );

INSERT INTO Peliculas (codigo, titulo) VALUES 
  ('1234567890121', 'Titanic'),
  ('1234567890122', 'John Wick'),
  ('1234567890123', 'Panceta Azul en Vinagre');



CREATE OR REPLACE TABLE alquila(
  socio int NOT NULL,
  pelicula varchar(13),
  PRIMARY KEY (socio, pelicula),
  FOREIGN KEY (socio) REFERENCES Socios(codigo),
  FOREIGN KEY (pelicula) REFERENCES Peliculas(codigo)
  );

CREATE OR REPLACE TABLE alquilaFechas (
  socio int NOT NULL,
  pelicula varchar(13),
  fecha date,
  PRIMARY KEY (socio, pelicula, fecha),
  FOREIGN KEY (socio, pelicula) REFERENCES alquila (socio, pelicula)
  );

INSERT INTO alquila (socio, pelicula) VALUES 
  (1, '1234567890122'),
  (2, '1234567890121'),
  (3, '1234567890123');

INSERT INTO alquilaFechas (socio, pelicula, fecha) VALUES 
  (1, 1234567890122, 1978-04-15),
  (2, 1234567890121, 1982-05-14),
  (3, 1234567890123, 2019-06-055);
